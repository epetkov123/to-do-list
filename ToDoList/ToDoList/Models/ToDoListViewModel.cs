﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ToDoList.Models
{
    public class ToDoListViewModel
    {
        [Required]
        public int Id { get; set; }
        [Required(ErrorMessage = "Needed")]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public DateTime DateCreated { get; set; }
        [Required]
        public DateTime DueDate { get; set; }
    }
}
