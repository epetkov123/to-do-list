﻿using Microsoft.AspNetCore.Mvc;
using ToDoList.Models;
using ToDoList.Service.Service;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using System.Collections.Generic;
using System.Linq;
using ToDoList.Data.Entity;

namespace ToDoList.Controllers
{
    public class HomeController : Controller
    {
            private IToDoListService service;

            private readonly IMapper mapper;

            public HomeController(IToDoListService service, IMapper mapper)
            {
                this.service = service;
                this.mapper = mapper;
            }

        [HttpGet]
        public IActionResult Index()
        {
            IList<ToDoListViewModel> model = service.GetAll()
                .ProjectTo<ToDoListViewModel>().ToList();

            return View(model);
        }

        [HttpGet]
        public IActionResult GetById(int id)
        {
            var item = mapper.Map(service.GetById(id), new ToDoListViewModel());

            return View("View", item);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(ToDoListViewModel model)
        {
            var item = mapper.Map(model, new ToDoListEntity());

            service.Insert(item);
            service.Save();

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            service.Delete(id);
            service.Save();

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var item = mapper.Map(service.GetById(id), new ToDoListViewModel());    

            return View(item);
        }

        [HttpPost]
        public IActionResult Edit(ToDoListViewModel model)
        {
            var item = mapper.Map(model, new ToDoListEntity());

            service.Update(item);
            service.Save();

            return RedirectToAction("Index");
        }
    }
}