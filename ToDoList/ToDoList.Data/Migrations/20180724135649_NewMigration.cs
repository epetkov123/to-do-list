﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ToDoList.Data.Migrations
{
    public partial class NewMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "dueDate",
                table: "ToDoList",
                newName: "DueDate");

            migrationBuilder.RenameColumn(
                name: "dateCreated",
                table: "ToDoList",
                newName: "DateCreated");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "ToDoList",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "ToDoList",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "DueDate",
                table: "ToDoList",
                newName: "dueDate");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "ToDoList",
                newName: "dateCreated");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "ToDoList",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "ToDoList",
                nullable: true,
                oldClrType: typeof(string));
        }
    }
}
