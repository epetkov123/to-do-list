﻿using Microsoft.EntityFrameworkCore;
using ToDoList.Data.Entity;

namespace ToDoList.Data
{
    public class ToDoListContext : DbContext
    {
        public ToDoListContext(DbContextOptions<ToDoListContext> options)
            : base(options)
        {
        }

        public DbSet<ToDoListEntity> ToDoList { get; set; }
    }
}