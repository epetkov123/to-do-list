﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ToDoList.Data.Entity
{
    public class ToDoListEntity
    {
        [Required]
        public int Id { get; set; }
        [Required (ErrorMessage = "Needed")]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public DateTime DateCreated { get; set; }
        [Required]
        public DateTime DueDate { get; set; }
    }
}
