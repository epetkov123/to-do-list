﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using ToDoList.Data.Entity;

namespace ToDoList.Data.Repository
{
    public class ToDoListRepository : IToDoListRepository
    {
        protected ToDoListContext context;
        protected DbSet<ToDoListEntity> dbSet;

        public ToDoListRepository(ToDoListContext context)
        {
            this.context = context;
            dbSet = context.Set<ToDoListEntity>();
        }

        public void Insert(ToDoListEntity item)
        {
            dbSet.Add(item);
        }

        public IQueryable<ToDoListEntity> Get()
        {
            return dbSet;
        }

        public ToDoListEntity Get(int id)
        {
            return dbSet.Find(id);
        }

        public void Update(ToDoListEntity item)
        {
            dbSet.Update(item);
        }

        public void Delete(ToDoListEntity item)
        {
            dbSet.Remove(item);
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}
