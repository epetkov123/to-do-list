﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToDoList.Data.Entity;

namespace ToDoList.Data.Repository
{
    public interface IToDoListRepository
    {
        void Insert(ToDoListEntity item);

        IQueryable<ToDoListEntity> Get();

        void Update(ToDoListEntity item);

        void Delete(ToDoListEntity item);

        ToDoListEntity Get(int id);

        void Save();
    }
}
