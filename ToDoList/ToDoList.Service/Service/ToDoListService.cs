﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToDoList.Data;
using ToDoList.Data.Entity;
using ToDoList.Data.Repository;

namespace ToDoList.Service.Service
{
    public class ToDoListService : IToDoListService
    {
        private IToDoListRepository repository;

        public ToDoListService(IToDoListRepository repository)
        {
            this.repository = repository;
        }

        public void Insert(ToDoListEntity item)
        {
            item.DateCreated = DateTime.Now;
            repository.Insert(item);
        }

        public IQueryable<ToDoListEntity> GetAll()
        {
            return repository.Get();
        }

        public void Update(ToDoListEntity item)
        {
            item.DateCreated = DateTime.Now;
            repository.Update(item);
        }

        public void Delete(int id)
        {
            repository.Delete(repository.Get(id));
        }

        public void Save()
        {
            repository.Save();
        }

        public ToDoListEntity GetById(int id)
        {
            return repository.Get(id);
        }
    }
}