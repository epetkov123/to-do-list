﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToDoList.Data.Entity;

namespace ToDoList.Service.Service
{
    public interface IToDoListService
    {
        void Insert(ToDoListEntity item);

        IQueryable<ToDoListEntity> GetAll();

        void Update(ToDoListEntity item);

        ToDoListEntity GetById(int id);

        void Save();

        void Delete(int id);
    }
}
